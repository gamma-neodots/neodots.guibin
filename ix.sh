#!/usr/bin/env sh
[ -f "$HOME/.netrc" ] && opts='-n'
while getopts ":hd:i:n:" opt; do
	case "$opt" in
		d)
			# shellcheck disable=2086
			curl $opts -X DELETE ix.io/$OPTARG
			exit $?
			;;
		i) opts="$opts -X PUT"; id="$OPTARG" ;;
		n) opts="$opts -F read:1=$OPTARG" ;;
		h)
			cat >&2 << EOF
$(basename "$0") [-d ID] [-i ID] [-n N] [curl opts]

	-d ID   delete the given ID
	-i ID   replace the given ID
	-n N    delete the paste after N reads
	-h      show this help

Curl can work with a filename or stdin. If a filename is
provided, ix.io will set the name on the paste. Otherwise,
ix will read from stdin.
EOF
			exit 0 ;;
		*) exit 1 ;;
	esac
done
shift $((OPTIND - 1))
if [ -t 0 ]; then
	file="$1"
	shift
	# shellcheck disable=2086
	[ -n "${file:-}" ] && exec curl $opts -F "f:1=@$file" "$@" "ix.io/$id"
	echo >&2 "Reading from stdin. ^C to cancel, ^D to send."
fi
# shellcheck disable=2086
curl $opts -F f:1='<-' "$@" "ix.io/$id"
