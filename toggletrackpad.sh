#!/usr/bin/env sh
set -eu

IFS="
"
if [ -n "${WAYLAND_DISPLAY+x}" ]; then
	swaymsg input "$(swaymsg -t get_inputs |
		jq -r '.[]|first(select(.identifier|contains("Touchpad")).identifier)')" \
		events toggle enabled disabled
else
	dev="$(xinput list --name-only | grep Touchpad)"
	# shellcheck disable=2015
	xinput list-props "$dev" | grep -q 'Device Enabled.*1$' &&
		exec xinput disable "$dev" || exec xinput enable "$dev"
fi
