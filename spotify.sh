#!/usr/bin/env bash
# Automatically mute Spotify in Pulseaudio
# if it reports a trackid matching "spotify:ad:*"
enable_fj=1
delay=1
set_delay() {
	shopt -s extglob
	[[ $1 == +([[:digit:]])?(.)*([[:digit:]]) ]] \
		&& delay="$1"
}

while
	case $1 in
		--debug|--xtrace|-x)
			exec 2> >(ts -s > "$XDG_RUNTIME_DIR/spotify.log")
			set -x
		;;
		--delay=*)
			set_delay "${1#*=}"
		;;
		-d|--delay)
			shift
			set_delay "$1"
		;;
		--disable-firejail)
			enable_fj=0
		;;
		--help|-h)
			cat >&2 << EOF
Usage: $0 [ flags ] [ -- ] [ spotify args ]

Flags:
--debug   -d   -x   --xtrace 
	enable timestamped xtrace, save to $XDG_RUNTIME_DIR/spotify.log

--disable-firejail
	disables firejail

--delay=N.N   -d N.N
	set between reading the next track and muting/unmuting
	defaults to 1
EOF
			exit 0
		;;
		--)
			shift
			break
		;;
		*)
			break
		;;
	esac
	shift
do :; done
if (( enable_fj )) && type firejail > /dev/null; then
	firejail --quiet /opt/spotify/spotify "$@" &
else
	/opt/spotify/spotify "$@" &
fi
pid=$!
while
	sleep 60
	si="$(pacmd list-sink-inputs | awk '/index:/{si = $2}; /"Spotify"$/{print si; exit}')"
	if [[ -n "${si:-}" ]]; then
		playerctl --player spotify --follow metadata mpris:trackid |
		while read -r id; do
			sleep "$delay" # it seems like the metadata changes ~1s before the track changes
			if [[ $id == spotify:ad:* ]]; then
				pactl set-sink-input-mute "$si" 1
			else
				pactl set-sink-input-mute "$si" 0
			fi
		done
	fi
	echo >&2 "Unable to find a sink-input for Spotify."
do :; done &
wait "$pid"
pgroup="$(ps -o pgid= $$)"
pgroup="${pgroup// /}"
setsid kill -- -"$pgroup"
exit 0
