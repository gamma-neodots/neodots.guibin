#!/bin/sh
set -x
IFS='
	'

# -- run in i3 --
if [ -z "${XDG_SESSION_ID:-}" ]; then
	#shellcheck source=../i3/LIB/i3/i3tool.sh
	XDG_RUNTIME_DIR="${XDG_RUNTIME_DIR:-/run/user/$(id -u)}"
	. "$HOME/.local/lib/i3/i3tool.sh"
	i3tool -n exec "'$(realpath "$0")'" "$@"
	s="$?"
	exit "$s"
fi

# already awake? `touch ~/.noalarm`
if [ -f "$HOME/.noalarm" ]; then
	# exit if within last 12h (43200s)
	[ "$(( $(date '+%s') - $(stat -c '%Y' "$HOME/.noalarm") ))" -lt 43200 ] && exit 0
fi

# delay or error out
sleep "$1" 2> /dev/null || : 

msg="It is $(date +%R), ${ALARMMSG:-'get up!'}"

player() {
	s="$(pacmd dump | awk '/^set-default-si/{print $2}')"
	si="$(pacmd list-sink-inputs | awk '/index:/{si = $2}; /media.role = "music"/{print si; exit}')"
	pacmd set-sink-mute "$s" false
	#pacmd set-sink-volume "$s" 0x10000
	pacmd set-sink-input-mute "$si" false
	pacmd set-sink-input-volume "$si" "${1:-0x10000}"
	[ "$(playerctl status)" = "Playing" ] && return 0
	eval "${PLAYER:-playerctl play}"
	playerctl volume 1.0
}

player "$2" &

notify-send "$msg" --app-name=Alarm &

if command -v pico2wave > /dev/null ; then
	trap 'rm "$ttsfile"' INT EXIT
	ttsfile="$(mktemp --suffix=.wav)"
	# 8:05 == "eight oh-five"
	echo "$msg" | sed 's/:0/ o /' | pico2wave -w "$ttsfile"
	paplay "$ttsfile"
	rm "$ttsfile"
	trap - INT EXIT
fi
